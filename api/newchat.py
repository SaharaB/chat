from sqlalchemy import create_engine
from flask import Flask, jsonify, request

engine = create_engine("mysql+pymysql://{user}:{pw}@{host}/{db}"
                       .format(user="root",
                               host='localhost',
                               pw="Musset15",
                               db="box_chat"))

app = Flask(__name__)

@app.route("/aff_users")
def bdd():
     connection = engine.connect()
     query = "SELECT * FROM users "
     result = connection.execute(query)

     return jsonify({'result': [dict(row) for row in result]})

@app.route("/new_user")
def ajout_user():
     connection = engine.connect()
     # user_id = request.args.get('pseudo', type=int)
     last_name = request.args.get('last_name', type=str)
     first_name = request.args.get('first_name', type=str)
     pseudo = request.args.get('pseudo', type=str)
     password = request.args.get('password', type=str)
     email = request.args.get('email' , type=str)
     query = 'INSERT INTO users (user_id,last_name,first_name,pseudo,password\
          ,email,registration_date) VALUES(NULL,"{0}","{1}","{2}","{3}","{4}",now())'.format(last_name,first_name,pseudo,password,email)
     result = connection.execute(query)
     return 'new_user_created'

@app.route("/limit")
def limit():
     connection = engine.connect()
     query = 'SELECT * FROM users LIMIT 10'
     result = connection.execute(query)
     return jsonify({'result': [dict(row) for row in result]})

@app.route("/message")
def msg():
     connection = engine.connect()
     query = 'SELECT * FROM messages LIMIT 4'
     result = connection.execute(query)
     return jsonify({'result': [dict(row) for row in result]})

@app.route("/add")
def ajout_msg():
     connection = engine.connect()
     user_id = request.args.get('user_id', type=int)
     message = request.args.get('message', type=str)
     query = 'INSERT INTO messages(message_id,message,send_date,user_id) VALUES(0,"right now it is ok",now(),6)'.format(message,user_id)
     result = connection.execute(query)
     return 'message inséré avec succes'

@app.route("/delete")
def del_users():
     connection = engine.connect()
     user_id = request.args.get('user_id', type=int)
     query = 'DELETE from users WHERE user_id = "10"'.format(user_id)
     connection.execute(query)
     return "del"

@app.route("/update")
def update_user():
     connection = engine.connect()
     user_id = request.args.get('user_id', type=int)
     pseudo = request.args.get('pseudo', type=str)
     last_name = request.args.get('last_name', type=str)
     first_name = request.args.get('first_name', type=str)
     password = request.args.get('password', type=str)
     email = request.args.get('email' , type=str)
     query = 'UPDATE users SET first_name = "Mathildek" , last_name = "krill" , email = "mathildek@gmail.fr" , password = "********" , pseudo ="mathildekkar"  WHERE user_id = "7" '.format(last_name,first_name,pseudo,password,email,user_id)
     result = connection.execute(query)
     return 'update succes'